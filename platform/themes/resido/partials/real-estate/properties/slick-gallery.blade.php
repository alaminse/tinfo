<div class="featured_slick_gallery-slide">
    @foreach ($property->images as $index => $image)
        <div class="featured_slick_padd" style="display: none;">
            <a href="{{ RvMedia::getImageUrl($image, null, false, RvMedia::getDefaultImage()) }}" class="mfp-gallery">
                <img src="{{ RvMedia::getImageUrl($image, 'property_large', false, RvMedia::getDefaultImage()) }}"
                    class="img-fluid mx-auto" alt="{{ $property->name }}-{{ $index }}" />
            </a>
        </div>
    @endforeach
</div>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>

<script>
$(document).ready(function () {
    $('.featured_slick_gallery-slide').slick({
        // Slick Carousel configuration options here
        slidesToShow: 2,
        autoplay: true,
        autoplaySpeed: 2000,
        dots: true,
    });

    // Show each slide one by one after the Slick Carousel is initialized
    $('.featured_slick_padd').each(function (index, element) {
        setTimeout(function () {
            $(element).css('display', 'block');
        }, index * 100); // Adjust the delay as needed
    });
});
</script>
