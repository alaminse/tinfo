@php
    $layout = theme_option('properties_page_layout');
    $requestLayout = request()->input('layout');
    if ($requestLayout && in_array($requestLayout, array_keys(get_properties_page_layout()))) {
        $layout = $requestLayout;
    }

    $layout = ($layout && in_array($layout, array_keys(get_properties_page_layout()))) ? $layout : 'sidebar';
    $viewType = request()->input('view', 'grid');
    $gridClass = 'col-lg-12 col-md-12';
    $gridItemClass = 'col-lg-6 col-md-12';

    if ($layout !== 'full' && $layout !== 'grid_full') {
        $gridClass = 'col-lg-8 col-md-12';
    }

    switch ($layout) {
        case 'grid_sidebar':
        case 'grid_map':
        case 'sidebar':
        case 'map':
            if($viewType == 'list') {
                 $gridItemClass = 'col-lg-12 col-md-12';
            }
            break;

        case 'full':
            $viewType = 'list';
            break;

        case 'grid_full':
            if ($viewType == 'list') {
                $gridItemClass = 'col-lg-6 col-md-12';
            } else {
                $gridItemClass = 'col-lg-4 col-md-6 col-sm-12';
            }
            break;
    }
@endphp

@if ($layout == 'half_map')
    @php
        Theme::asset()
            ->usePath()
            ->add('leaflet-css', 'plugins/leaflet.css');
        Theme::asset()
            ->container('footer')
            ->usePath()
            ->add('leaflet-js', 'plugins/leaflet.js');
        Theme::asset()
            ->container('footer')
            ->usePath()
            ->add('leaflet.markercluster-src-js', 'plugins/leaflet.markercluster-src.js');
    @endphp
    <div class="half-map container-fluid max-w-screen-2xl">
        <div class="fs-content">
            <form action="{{ route('public.properties') }}" method="get" id="ajax-filters-form">
                <input type="hidden" name="page" data-value="{{ $properties->currentPage() }}">
                <input type="hidden" name="layout" value="{{ request()->input('layout') }}">
                <div class="row">
                    <div class="fs-inner-container1 col-md-7" id="properties-list">
                        @include(Theme::getThemeNamespace('views.real-estate.includes.filters-halfmap'))
                        <div class="list-layout data-listing position-relative">
                            {!! Theme::partial('real-estate.properties.items', compact('properties')) !!}
                        </div>
                    </div>
                    <div class="fs-left-map-box1 col-md-5">
                        <div class="rightmap h-100">
                            <div id="map" data-type="{{ request()->input('type') }}"
                                 data-url="{{ route('public.ajax.properties.map') }}"
                                 data-center="{{ json_encode([43.615134, -76.393186]) }}"></div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="clearfix"></div>
@else
    <!-- ============================ All Property ================================== -->
    <section class="gray">
        <div class="container">

            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="filter_search_opt">
                        <a href="javascript:void(0);" class="open_search_menu">{{ __('Search Property') }}<i
                                class="ml-2 ti-menu"></i></a>
                    </div>
                </div>
            </div>

            <div class="row">
                @if ($layout !== 'full' && $layout !== 'grid_full')
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <div class="simple-sidebar sm-sidebar" id="filter_search" style="left: -310px;">
                            @include(Theme::getThemeNamespace('views.real-estate.includes.search-sidebar'))
                        </div>
                    </div>
                @endif

                <div class="{{ $gridClass }} list-layout">
                    <div class="row justify-content-center">
                        @include(Theme::getThemeNamespace('views.real-estate.includes.sorting-box'))
                    </div>

                    <div class="row">
                        @foreach ($properties as $property)
                            <div class="{{ $gridItemClass }}">
                                @if (strpos($viewType, 'grid') !== false)
                                    {!! Theme::partial('real-estate.properties.item-grid', compact('property')) !!}
                                @else
                                    {!! Theme::partial('real-estate.properties.item-list', compact('property')) !!}
                                @endif
                            </div>
                            <!-- End Single Property -->
                        @endforeach
                    </div>

                    <!-- Pagination -->
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <nav class="d-flex justify-content-center pt-3" aria-label="Page navigation">
                                {!! $properties->withQueryString()->onEachSide(1)->links() !!}
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            
            

            <div class="row mt-4">
                @if(request()->is('properties'))
                    <div class="page-content">
                    <h2>Flat, Apartment, Shop and Office for Rent and Sale in Dhaka, Bangladesh</h2>
                    <p>Dhaka is a city teeming with opportunities, and it is a magnet for businesses of all scales. Dhaka dynamic environment fosters a flourishing ecosystem for commerce, attracting not only startups but also well-established corporations. Bangladesh is a shining symbol of business success thanks to its strong economy, skilled workforce, and strategic location.</p>
                    <h3>Office Rent in Dhaka</h3>
                    <p>There are many office space options available in Dhaka. From modern high-rises in the bustling city center to tranquil suburban settings, Dhaka has a lot to offer. Find offices that meet your business needs, whether you want a corporate space in the busy city center or a peaceful location for creative projects. Or a serene location for creative endeavors.</p>
                    <h3>House Rent in Dhaka</h3>
                    <p>We offer various housing choices for people searching for a place to live. Whether you like living in the city or in quieter suburbs, you can find flats and apartments that suit your lifestyle. Dhaka residential offerings are as diverse as its residents.</p>
                    <h3>Shop Space Rent in Dhaka</h3>
                    <p>The vibrant city of Dhaka offers a dynamic marketplace for entrepreneurs and retailers. The city shop spaces cater to various business ventures, from retail boutiques to specialty stores. Gulshan and Dhanmondi are promising locations to establish a store and captivate a large customer base.</p>
                    <h4>Our Service Area</h4>
                    <p>Tinfo Property proudly serves Dhaka most coveted areas, including <a href="https://tinfoproperty.com/dhaka/gulshan">Gulshan</a>, <a href="https://tinfoproperty.com/dhaka/banani">Banani</a>, <a href="https://tinfoproperty.com/dhaka/tejgaon">Tejgaon</a>, <a href="https://tinfoproperty.com/dhaka/dhanmondi">Dhanmondi</a>, <a href="https://tinfoproperty.com/dhaka/uttara">Uttara</a>, <a href="https://tinfoproperty.com/dhaka/baridhara">Baridhara</a>, <a href="https://tinfoproperty.com/dhaka/mohammadpur">Mohammadpur</a> and <a href="https://tinfoproperty.com/dhaka/mirpur">Mirpur</a>. These thriving neighborhoods are the heart of commerce, culture, and lifestyle in the capital city of Bangladesh. Whether you want an office space in Gulshan or an apartment in Dhanmondi, we can help you. We know these areas very well, so we can find the ideal property for you. Tinfo Property is your trusted partner in navigating the vibrant real estate landscape of Dhaka most sought-after areas.</p>
                    <h4>Why Choose Tinfo Property for Property Buy, Sale and Rent?</h4>
                    <p>Here at Tinfo Property, we fully comprehend the significance of discovering the ideal property for your business or residential needs. Our commitment to excellence, industry expertise, and customer-centric approach set us apart:</p>
                    <ul>
                     	<li>We have an extensive array of properties to choose from, including offices, flats, apartments, and shops. Properties to choose from, including offices, flats, apartments, and shops. We have carefully curated our portfolio to cater to the specific needs and desires of our esteemed clients.</li>
                     	<li>We know a lot about the real estate market in Dhaka. We can help you find the perfect property where you want it.</li>
                     	<li>We focus on your needs and preferences to help you find the right property for you.</li>
                     	<li>Transparency and Trust: Our business is built on the fundamental principle of transparency. We provide clients with clear and genuine information about properties and rental agreements, building trust. </li>
                     	<li>Ease of Contact: Contact us anytime for assistance or inquiries. Our mission is to ensure that your property search and rental process is smooth and highly efficient.</li>
                    </ul>
                    <strong>Contact Us for Service</strong>

                    <p>Are you prepared to begin your quest for the <a href="https://tinfoproperty.com/property-buy-sell-rental-agent-dhaka">perfect property in Dhaka</a>? Feel free to reach out to Tinfo Property for top-notch real estate services. We are here to help you find the perfect flat, apartment, shop space, or office for rent or sale in Dhaka. Your satisfaction is our priority, and we look forward to assisting you in your property endeavors.</p>
                    </div>
                    
                @elseif(!empty($data['description']))
                    {!! $data['description'] !!}
                @endif
                
                
            </div>            
        </div>
    </section>
@endif

<script id="traffic-popup-map-template" type="text/x-custom-template">
    {!! Theme::partial('real-estate.properties.map-popup', ['property' => get_object_property_map()]) !!}
</script>
