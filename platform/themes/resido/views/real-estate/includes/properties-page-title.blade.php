@php
    $layout = theme_option('properties_page_layout');

    $requestLayout = request()->input('layout');
    if ($requestLayout && in_array($requestLayout, array_keys(get_properties_page_layout()))) {
        $layout = $requestLayout;
    }

    $layout = ($layout && in_array($layout, array_keys(get_properties_page_layout()))) ? $layout : 'sidebar';
@endphp

<script>
    fbq('track', 'ViewContent', {
  content_type: 'property_list'
});
</script>
@if($layout == 'full')
    <div class="page-title">
        </div>
@elseif($layout == 'grid_map' || $layout == 'map')
    @php
        Theme::asset()
            ->usePath()
            ->add('leaflet-css', 'plugins/leaflet.css');
        Theme::asset()
            ->container('footer')
            ->usePath()
            ->add('leaflet-js', 'plugins/leaflet.js');
        Theme::asset()
            ->container('footer')
            ->usePath()
            ->add('leaflet.markercluster-src-js', 'plugins/leaflet.markercluster-src.js');
    @endphp
    <div class="home-map-banner full-wrapious">
        <div class="hm-map-container fw-map">
            <div id="map" data-type="{{ request()->input('type') }}"
                 data-url="{{ route('public.ajax.properties.map') }}"
                 data-center="{{ json_encode([43.615134, -76.393186]) }}"></div>
        </div>

    </div>
    <script id="traffic-popup-map-template" type="text/x-custom-template">
        {!! Theme::partial('real-estate.properties.map-popup', ['property' => get_object_property_map()]) !!}
    </script>
@else
    <div class="page-title">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <h1 class="ipt-title">
                    @if(!empty($data['name']))
                        All Properties For Rent & Sale in {!! $data['name'] !!}
                    @else
                        {{ __('All Properties For Rent & Sale in Dhaka') }}
                    @endif
                    </h1>
                    <span class="ipn-subtitle">{{ theme_option('properties_description') }}</span>

                </div>
            </div>
        </div>
    </div>
@endif
