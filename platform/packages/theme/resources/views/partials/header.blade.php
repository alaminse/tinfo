{!! SeoHelper::render() !!}

@if ($favicon = theme_option('favicon'))
    <!--<link rel="shortcut icon" type="image/x-icon" href="{{ RvMedia::getImageUrl($favicon) }}">-->
    
    <!--code for favicon-->
<link rel="shortcut icon" type="image/x-icon" href="https://tinfoproperty.com/favicon.ico">
<link rel="apple-touch-icon" sizes="180x180" href="https://tinfoproperty.com/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="https://tinfoproperty.com/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="https://tinfoproperty.com/favicon-16x16.png">
<link rel="manifest" href="https://tinfoproperty.com/site.webmanifest">
<link rel="mask-icon" href="https://tinfoproperty.com/safari-pinned-tab.svg" color="#5bbad5">
<meta name="msapplication-TileColor" content="#da532c">
<meta name="theme-color" content="#ffffff">
    <!--end code for favicon-->
@endif

@if (Theme::has('headerMeta'))
    {!! Theme::get('headerMeta') !!}
@endif

{!! apply_filters('theme_front_meta', null) !!}

<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "WebSite",
  "name": "{{ rescue(fn() => SeoHelper::openGraph()->getProperty('site_name')) }}",
  "url": "{{ url('') }}"
}
</script>

{!! Theme::asset()->styles() !!}
{!! Theme::asset()->container('after_header')->styles() !!}
{!! Theme::asset()->container('header')->scripts() !!}

{!! apply_filters(THEME_FRONT_HEADER, null) !!}

<script>
    window.siteUrl = "{{ route('public.index') }}";
</script>
