<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('re_properties', function (Blueprint $table) {
            $table->string('property_code')->unique();
            $table->string('building_name')->nullable();
            $table->string('owner_name')->nullable();
            $table->string('owner_number')->nullable();
            $table->string('contact_person_name')->nullable();
            $table->string('contact_person_number')->nullable();
            $table->text('full_address')->nullable();
            $table->text('additional_info')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('re_properties', function (Blueprint $table) {
            $table->dropColumn('property_code');
            $table->dropColumn('building_name');
            $table->dropColumn('owner_name');
            $table->dropColumn('owner_number');
            $table->dropColumn('contact_person_name');
            $table->dropColumn('contact_person_number');
            $table->dropColumn('full_address');
            $table->dropColumn('additional_info');
        });
    }
};
