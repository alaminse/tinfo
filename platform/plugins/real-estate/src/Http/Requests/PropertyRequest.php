<?php

namespace Botble\RealEstate\Http\Requests;

use Botble\RealEstate\Enums\ModerationStatusEnum;
use Botble\Support\Http\Requests\Request;
use Illuminate\Validation\Rule;

class PropertyRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'property_code' => 'required|string|unique:re_properties,property_code,' . $this->route('property'),
            'building_name' => 'string|nullable',
            'owner_name' => 'string|nullable',
            'owner_number' => 'string|nullable',
            'contact_person_name' => 'string|nullable',
            'contact_person_number' => 'string|nullable',
            'full_address' => 'string|nullable',
            'additional_info'=> 'string|nullable',
            'description' => 'max:350',
            'content' => 'required',
            'number_bedroom' => 'numeric|min:0|max:10000|nullable',
            'number_bathroom' => 'numeric|min:0|max:10000|nullable',
            'number_floor' => 'numeric|min:0|max:10000|nullable',
            'price' => 'numeric|min:0|nullable',
            'latitude' => ['max:20', 'nullable', 'regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],
            'longitude' => [
                'max:20',
                'nullable',
                'regex:/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/',
            ],
            'moderation_status' => Rule::in(ModerationStatusEnum::values()),
        ];
    }
}
